package com.shujrah.waqar.myapplication

import android.database.Cursor
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_quran_read.*

class QuranRead : AppCompatActivity() {
    val TAG = "QuranReadActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quran_read)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }


        var dbHelper:DBHelper = DBHelper(applicationContext)
//        println("SD ok? ${dbHelper.checkDB()}" )
        //dbHelper.getSurah(1)
        //dbHelper.createDatabase()
        dbHelper.copy()

        var readingArea: TextView = findViewById(R.id.readingArea) as TextView
        

        val surahList:Cursor = dbHelper.query("select * from quran_arab limit 100")

        val largeString = StringBuilder()
        largeString.append("")
        
        while (surahList.moveToNext()){
//            Log.i(TAG, surahList.getString(surahList.getColumnIndex("text")))
            largeString.append(surahList.getString(surahList.getColumnIndex("text")))
            largeString.append("\n")
        }

        readingArea.text = largeString

//        dbHelper.openDatabase()
//        dbHelper.readSurah(110)

    }
}
