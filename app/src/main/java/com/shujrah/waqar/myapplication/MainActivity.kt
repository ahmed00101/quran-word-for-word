package com.shujrah.waqar.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import android.content.Intent



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this, QuranRead::class.java)
        startActivity(intent)

        val button: Button = findViewById(R.id.button)
//button.setOnClickListener{
//    println("button clicked using listener")
//}

    }

    fun clicked(v: View){
        println("using onClick property")
    }

    fun readQuran(view: View){

        Toast.makeText(applicationContext, "Toasted. switching to different activity", Toast.LENGTH_SHORT).show()

        val intent = Intent(this, QuranRead::class.java)
        startActivity(intent)
    }
}
