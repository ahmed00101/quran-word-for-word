package com.shujrah.waqar.myapplication

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class DBHelper(var context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
     var db: SQLiteDatabase? = null
    val TAG = "Database"

    companion object {
        private val DATABASE_NAME = "alquran.db"
        private val DATABASE_VERSION = 1
        private var DB_PATH = "databases/"
        private val ENCODING_SETTING = "PRAGMA encoding ='UTF-8'"
    }

    override fun onCreate(sQLiteDatabase: SQLiteDatabase) {}

    override fun onUpgrade(sQLiteDatabase: SQLiteDatabase, i: Int, i2: Int) {}

    init {
        val stringBuilder = StringBuilder()
//        stringBuilder.append("/data/data/")
        stringBuilder.append(context.filesDir.path)

//        stringBuilder.append(context.packageName)
        stringBuilder.append("/databases/")
        DB_PATH = stringBuilder.toString()
    }

    override fun onOpen(sQLiteDatabase: SQLiteDatabase) {
        if (!sQLiteDatabase.isReadOnly) {
            sQLiteDatabase.execSQL(ENCODING_SETTING)
        }
    }

    fun openDatabase() {
        val stringBuilder = StringBuilder()
        stringBuilder.append(DB_PATH)
        stringBuilder.append(DATABASE_NAME)
        try {
//            this.db = SQLiteDatabase.openDatabase(stringBuilder.toString(), null, 1)
            this.db = SQLiteDatabase.openDatabase(context.getFileStreamPath("alquran.db").path, null, 1)


            Log.i(TAG, "DB with path ${stringBuilder.toString()} is now open")
        } catch (unused: SQLiteException) {
            Log.i(TAG, "BD is not found at ${stringBuilder.toString()}")
            //throw Error("Error opening database")
        }

    }


    fun checkDatabase(): Boolean {
        var sQLiteDatabase: SQLiteDatabase? = null
        try {
            val stringBuilder = StringBuilder()
            stringBuilder.append(DB_PATH)
            stringBuilder.append(DATABASE_NAME)

            sQLiteDatabase = SQLiteDatabase.openDatabase(stringBuilder.toString(), null, 1)
        } catch (unused: SQLiteException) {
            Log.e(TAG, "DB: doesn't exist")
        }

        if (sQLiteDatabase == null) {
            return false
        }
        sQLiteDatabase.close()
        return true
    }

    fun copyDatabase() {
        try {
            println("trying to copy database")
            val open = this.context.assets.open(DATABASE_NAME)

            val stringBuilder = StringBuilder()
            stringBuilder.append(DB_PATH)
            stringBuilder.append(DATABASE_NAME)
            println("output: ${stringBuilder.toString()}")

            val fileOutputStream = FileOutputStream(stringBuilder.toString())
            val bArr = ByteArray(1024)
            while (true) {
                val read = open.read(bArr)
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read)
                } else {
                    fileOutputStream.flush()
                    fileOutputStream.close()
                    open.close()
                    return
                }
            }
        } catch (unused: IOException) {
            Log.e("copy", "error")
            throw Error("Error creating database")
        }

    }

    fun createDatabase() {
        if (checkDatabase()) {
            Log.i(TAG, "DB: exist")
            return
        }
        readableDatabase
        copyDatabase()
    }


    @Synchronized
    override fun close() {
        if (this.db != null) {
            this.db!!.close()
        }
        super.close()


    }



    fun readSurah(i: Int = 1){
        println("is db open? ${db?.isOpen}")
        var q= "select * from 'surat'"
        q = "SELECT name FROM sqlite_master WHERE type ='table'"


        var cursor: Cursor


        try{
            cursor = db?.rawQuery(q, null)!!
        }catch (e: SQLiteException){
            Log.e(TAG, e.message)
        }
        cursor = db?.rawQuery(q, null)!!

        Log.i(TAG,"result size: ${cursor.count}")


        q = "SELECT name FROM sqlite_master WHERE type ='table'"
         cursor= db?.rawQuery(q, null)!!
        println("tables q result size: ${cursor?.count}")


        //Log.i(TAG, c?.getString(c?.getColumnIndex("name")))

        if(cursor != null){
            if(cursor.moveToFirst()) {
            do{
                Log.i(TAG, cursor?.getString(cursor?.getColumnIndex("name")))

            }while(cursor.moveToNext())

            }


        }else{
            println("c is null")
        }

    }


    fun copy() {
        val bufferSize = 1024
        val assetManager = context.assets
        val assetFiles = assetManager.list("")
//        val list: Array<String> = context.assets.list("")
//        if (list.size > 0){
//         for(file in list){
//             Log.e(TAG, file)
//         }
//        }
        //openDatabase()




        assetFiles.forEach {
            if(!it.contains(".",true)){
                Log.e(TAG, "Folder ${it.toString()} so skipping.")
                return@forEach
            }
           Log.e(TAG, "name: ${it.toString()}")
            val inputStream = assetManager.open(it)
            val outputStream = FileOutputStream(File(context.filesDir, it))

            try {
                inputStream.copyTo(outputStream, bufferSize)
                Log.i(TAG, "file: ${it.toString()} is copied")
            } finally {
                inputStream.close()
                outputStream.flush()
                outputStream.close()
            }

            openDatabase()
            //readSurah()
        }
    }


    fun query(q: String):Cursor{
    return db?.rawQuery(q,null)!!

    }
}//end of class